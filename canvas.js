//initialize
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

var mouse = false;
ctx.lineJoin = "round";
ctx.lineCap = "round";
var positionX, positionY;

//Element
var brush = document.getElementById("brush"); //brush 
var eraser = document.getElementById("erase"); //eraser 
var color = document.getElementById("myColor"); //color
var size = document.getElementById("myRange"); //size
var reset = document.getElementById("reset"); //reset
var saveLink = document.getElementById("saveLink"); //saveLink 
var upload = document.getElementById("upload");///upload

var stringvalue = document.getElementById("stringvalue");
var text = document.getElementById("text");

//initialize color
var myColor = color.value;
ctx.strokeStyle = myColor;
ctx.fillStyle = myColor;

//initialize size
var mySize = size.value;
ctx.lineWidth = mySize;

brush.style.border = "2px solid red";
canvas.style.cursor = "default";

canvas.addEventListener("mousedown", brushDown, false);
canvas.addEventListener("mousemove", brushMove, false);
canvas.addEventListener("mouseup", brushUp, false);

//Event Listeners 
brush.addEventListener("click", brushClick);
eraser.addEventListener("click", eraserClick);
color.addEventListener("change", colorChange);
size.addEventListener("change", sizeChange);
reset.addEventListener("click", resetClick);
saveLink.addEventListener("click", saveClick);
upload.addEventListener("click",uploadClick);
undo.addEventListener("click",cUndo);
redo.addEventListener("click",cRedo);
text.addEventListener("click",textclick);

//Color change
function colorChange() {
	myColor = color.value;
	ctx.strokeStyle = myColor;
	ctx.fillStyle = myColor;
}

//Size change
function sizeChange() {
	mySize = size.value;
	ctx.lineWidth = mySize;
}

//get coordinate
function getCoordinates(canvas, e) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: e.clientX - rect.left,
		y: e.clientY - rect.top
	};
}
//brush function
function brushDraw(canvas, positionX, positionY) {
	if(mouse) {
		ctx.lineTo(positionX, positionY);
		ctx.stroke();
		canvas.style.cursor = "url('Pencil.cur'),auto";
	}
}

function brushDown(e) {
	mouse = true;
	var coordinates = getCoordinates(canvas, e);
	canvas.style.cursor ="url('Pencil.cur'),auto";
	positionX = coordinates.x;
	positionY = coordinates.y;
	ctx.beginPath();
	ctx.moveTo(positionX, positionY);
	ctx.lineTo(positionX, positionY);
	ctx.stroke();
}

function brushMove(e) {
	var coordinates = getCoordinates(canvas, e);
	positionX = coordinates.x;
	positionY = coordinates.y;
	brushDraw(canvas, positionX, positionY);
}

function brushUp() {
	mouse = false;
	canvas.style.cursor ="default";	
	cPush();
}

function brushClick() {
	var brushColor = document.getElementById("myColor");
	ctx.strokeStyle = brushColor.value; 
	canvas.style.cursor = "url('Pencil.cur'),auto";
	brush.style.border = "2px solid red";
	eraser.style.border = "none";
	ctx.globalCompositeOperation="source-over";
	canvas.addEventListener("mousedown", brushDown, false); //bubble phase
	canvas.addEventListener("mousemove", brushMove, false);
	canvas.addEventListener("mouseup", brushUp, false);
	canvas.removeEventListener("mousedown",textdown,false);
	canvas.removeEventListener("mousemove",textmove,false);
    //canvas,addEventListener("mouseout",brushUp,false);
}

//Down below is for the eraser function
function brushDown1(e) {
	mouse = true;
	var coordinates = getCoordinates(canvas, e);
	canvas.style.cursor ="url('eraser.cur'),auto";
	positionX = coordinates.x;
	positionY = coordinates.y;
	ctx.beginPath();
	ctx.moveTo(positionX, positionY);
	ctx.lineTo(positionX, positionY);
	ctx.stroke();
}

function brushDraw1(canvas, positionX, positionY) {
	if(mouse) {
        
		ctx.lineTo(positionX, positionY);
		ctx.stroke();
		canvas.style.cursor ="url('eraser.cur'),auto";
	}
}

function brushMove1(e) {
	var coordinates = getCoordinates(canvas, e);
	positionX = coordinates.x;
	positionY = coordinates.y;
	brushDraw1(canvas, positionX, positionY);
}

//eraser 
function eraserClick() {
	canvas.style.cursor ="url('eraser.cur'),auto";
	ctx.strokeStyle = "white";
	eraser.style.border = "2px solid red";
	brush.style.border = "none";
	ctx.globalCompositeOperation="destination-out";
	canvas.addEventListener("mousedown", brushDown1, false);
	canvas.addEventListener("mousemove", brushMove1, false);
	canvas.addEventListener("mouseup", brushUp, false);
	canvas.removeEventListener("mousedown",textdown,false);
	canvas.removeEventListener("mousemove",textmove,false);
    //canvas,addEventListener("mouseout",brushUp,false);
}

//reset function 
function resetClick() {
	window.location.reload();
}

//save function 
function saveClick() {
	var data = canvas.toDataURL();//get image information
	console.log(data);
	saveLink.href = data;
	saveLink.download = "myImage.png";
}

//upload function
function uploadClick(){
    var upload = document.getElementById("upload");
    upload.addEventListener("change",uploadImage,false);

    function uploadImage(e){
        var reader = new FileReader();

        reader.onload = function(event){
            var img = new Image();
            img.onload = function () {
                ctx.drawImage(img,0,0,400,300);
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(e.target.files[0]);
	}
}

//below is the undo/redo function
var cPushArray = new Array();
var cStep = -1;

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
}
function cUndo() {
	if(cStep == 0){
		ctx.clearRect(0,0,canvas.width,canvas.height);
	}
    if (cStep > 0) {
        cStep--;
		var img = new Image();

		img.setAttribute("crossOrigin","Anonymous");
		
		img.onload = function () {
			ctx.globalCompositeOperation="copy";
			ctx.drawImage(img,0,0);
			ctx.globalCompositeOperation="source-over";
		};
		img.src = cPushArray[cStep];
	};
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var img = new Image();
		
		img.setAttribute("crossOrigin","Anonymous");
		
        img.onload = function () { 
			ctx.globalCompositeOperation="copy";
			ctx.drawImage(img, 0, 0);
			ctx.globalCompositeOperation="source-over";
		};
		img.src = cPushArray[cStep];
    };
}

//below is text function
function textdraw(positionX,positionY){
	canvas.style.cursor ="url('text.cur'),auto";
    ctx.fillstyle = "black";
    ctx.font = mySize + 'px fantasy';
    ctx.fillText(document.getElementById("stringvalue").value,positionX,positionY);
}

function textdown(e){
	canvas.style.cursor ="url('text.cur'),auto";
    mouse = true;
    var coordinates = getCoordinates(canvas,e);
    positionX = coordinates.x;
    positionY = coordinates.y;
    textdraw(positionX,positionY);
}

function textmove(e){
	canvas.style.cursor ="url('text.cur'),auto";
    mouse = false;
}

function textup(){
    mouse = false;
}

function textclick(){
	ctx.fillStyle = document.getElementById("myColor").value;
	canvas.style.cursor ="url('text.cur'),auto";
	ctx.globalCompositeOperation="source-over";
    canvas.addEventListener("mousedown", textdown, false); 
	canvas.addEventListener("mousemove",textmove,false);
	canvas.addEventListener("mouseup",textup,false);
	canvas.removeEventListener("mousedown",brushDown,false);
	canvas.removeEventListener("mousemove",brushMove,false);
	canvas.removeEventListener("mousedown",brushDown1,false);
	canvas.removeEventListener("mousemove",brushMove1,false);
}



