# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
**以下都是Button的功能實作**
1. 筆刷功能：
<img src="pencil.png"></img> <br>
點擊該按鈕之後即可在畫布上面畫畫，會根據現在所選的顏色進行畫畫<br>
2. 橡皮擦功能：
<img src="eraser.png"></img> <br>
點擊該按鈕之後即可在畫布上使用橡皮擦，被擦掉的地方會變成白色<br>
3. 下載功能：
<img src="download.png"></img><br>
點擊該按鈕之後即可下載當前畫布內的內容，會儲存為png檔<br>
4. Refresh功能：
<img src="reset.png"></img> <br>
點擊該按鈕之後即可清除當前畫布所有內容，變成一塊白布<br>
5. Redo功能：
<img src="redo.png"></img><br>
點擊該按鈕之後即可有重做的功能，可以回復到當前的畫布狀態<br>
6. Undo功能：
<img src="undo.png"></img><br>
點擊該按鈕之後即可有回復的功能，可以回到之前的畫布狀態<br>
7. 上傳功能：
<img src="upload.png"></img><br>
點擊該按鈕之後即可上傳之前儲存的畫布或者是圖片，會把上傳的檔案會出現在最左上角<br>

**以下是有關一些工具的實作功能**
*  更改顏色功能：
<img src="showcolor.png"></img><br>
點選之後會跑出所有顏色的table，可以選擇自己喜好的顏色，並會把所選顏色傳給brush和text<br>
*  更改尺寸功能：
<img src="showsize.png"></img><br>
這邊把筆刷尺寸和輸入文字的尺寸功能寫在一起，點選之後可以選擇size，並會傳給筆刷和文字<br>
*  輸入文字功能：
<img src="showtext.png"></img><br>
再輸入欄打上自己想要在畫布上面呈現的文字，打好之後，按下"Placee Text!"的按鈕，再到畫布上面任一處點擊滑鼠左鍵，就可以印下所打的文字<br>

**以下是轉換cursor的功能實作：**
*  在點選畫筆button之後會轉換成一個藍色鉛筆的cursor
*  在點選橡皮擦button之後會轉換成一個橡皮擦的cursor
*  在點選"Place Text!"button之後轉換成一個輸入的cursor
*  所有的轉換cursor只有在mousemove的時候會變，其餘cursor都是default
*  `canvas.style.cursor ="url('Pencil.cur'),auto";` 用左邊的式子還完成cursor的轉換
*  cursor的檔案為 cur.file


